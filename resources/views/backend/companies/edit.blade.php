@extends('layouts.master')

@section('title', 'Edit Company')

@push('css')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
@endpush

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('backend.dashboard') }}">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="{{ route('backend.companies.index') }}">Companies</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <span>Edit</span>
        </li>
    </ul>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title">Company
    <small>management</small>
</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="boxless tabbable-reversed">
            <div class="tab-content">
                <div class="tab-pane active" id="tab_0">
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-basket"></i>Edit Company </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"> </a>
                                <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                <a href="javascript:;" class="reload"> </a>
                                <a href="javascript:;" class="remove"> </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form action="{{ route('backend.companies.update', $company->slug) }}"
                            class="form-horizontal" method="POST" enctype="multipart/form-data">
                                {{ method_field('PUT') }}
                                @csrf
                                <div class="form-body">

                                    <div class="form-group{{ $errors->has('name') ? ' has-error ' : ''}}">
                                        <label for="name" class="col-md-3 control-label">Name</label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" placeholder="Company Name"
                                            name="name" value="{{ old('name') ? old('name') : $company->name }}" required>
                                            @if ($errors->has('name'))
                                                <span class="help-block"> {{ $errors->first('name') }} </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('email') ? ' has-error ' : ''}}">
                                        <label for="email" class="col-md-3 control-label">Email Address</label>
                                        <div class="col-md-4">
                                            <input type="email" class="form-control" placeholder="Email Address"
                                            name="email" value="{{ old('email') ? old('email') : $company->email }}" required>
                                            @if ($errors->has('email'))
                                            <span class="help-block"> {{ $errors->first('email') }} </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('website') ? ' has-error ' : ''}}">
                                        <label for="website" class="col-md-3 control-label">Website</label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" placeholder="Website"
                                            name="website" value="{{ old('website') ? old('website') : $company->website }}" required>
                                            @if ($errors->has('website'))
                                                <span class="help-block"> {{ $errors->first('website') }} </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('logo') ? ' has-error ' : ''}}">
                                        <label for="logo" class="col-md-3 control-label">Logo</label>
                                        <div class="col-md-4">
                                            <div class="input-group fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                    @if($company->logo)
                                                        <img src="{{ $company->image_path }}" alt="" />
                                                    @else
                                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                    @endif
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail"
                                                style="max-width: 200px; max-height: 150px;"> </div>
                                                <div>
                                                    <span class="btn default btn-file">
                                                        <span class="fileinput-new"> Select Logo </span>
                                                        <span class="fileinput-exists"> Change </span>
                                                        <input type="file" name="logo">
                                                    </span>
                                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                                                         Remove
                                                    </a>
                                                </div>
                                                @if ($errors->has('logo'))
                                                <span class="help-block"> {{ $errors->first('logo') }} </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-4">
                                            <button type="submit" class="btn green">Submit</button>
                                            <a type="button" class="btn default" href="{{ route('backend.companies.index') }}">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script>
$(document).ready(function() {
    $('.fileinput').fileinput();
});
</script>
@endpush
