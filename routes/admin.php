<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "admin" middleware group. Enjoy building your Admin!
|
*/

Route::get('dashboard', 'DashboardController@index')->name('dashboard');

Route::resource('users', 'UserController');

Route::resource('companies', 'CompanyController');

Route::resource('departments', 'DepartmentController');

Route::resource('employees', 'EmployeeController');

Route::resource('activitylogs', 'ActivityLogsController')->only(['index']);
