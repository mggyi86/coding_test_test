<?php

namespace App\Providers;

use App\Models\Company;
use App\Models\Department;
use App\Observers\CompanyObserver;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Blade;
use App\Observers\DepartmentObserver;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Company::observe(CompanyObserver::class);
        Department::observe(DepartmentObserver::class);
        Blade::directive('datetime', function ($expression) {
            return "<?php echo ($expression)->format('m/d/Y H:i') ?>";
        });
        Blade::if('superadmin', function() {
            return auth()->user()->isSuperAdmin();
        });
        if (app()->environment() == 'production') {
            URL::forceScheme('https');
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
