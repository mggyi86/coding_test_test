<?php

use App\Models\TransactionType;

if(! function_exists('transactionCode')) {
    function transactionTypeName($code) {
        $transactionType = TransactionType::code($code)->first();
        return $transactionType->name;
    }
}
