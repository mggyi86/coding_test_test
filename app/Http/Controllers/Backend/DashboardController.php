<?php

namespace App\Http\Controllers\Backend;

use App\Models\User;
use App\Models\Company;
use App\Models\Employee;
use App\Models\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users       = User::all()->count();
        $companies   = Company::all()->count();
        $employees   = Employee::all()->count();
        $departments = Department::all()->count();

        return view('backend.dashboard', compact(['users', 'companies', 'employees', 'departments']));
    }
}
