<?php

namespace App\Http\Requests\User;

use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,' . $this->route('user')->id ,
            // 'password' => 'required|string|min:6|confirmed',
            'phone' => 'required|phone:MM',
            'address' => 'nullable|string'
        ];
    }

    public function updateUser($user)
    {
        $user->name     = $this->name;
        $user->email    = $this->email;
        // $user->password = Hash::make($this->password);
        $user->phone    = $this->phone;
        $user->address  = $this->address;

        if($user->isDirty()) {
            $user->save();
        }
    }
}
