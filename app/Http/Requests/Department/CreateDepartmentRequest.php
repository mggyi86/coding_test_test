<?php

namespace App\Http\Requests\Department;

use App\Models\Company;
use Illuminate\Http\File;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Http\FormRequest;

class CreateDepartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company'    => 'required|integer|exists:companies,id',
            'name'       => 'required|string',
        ];
    }

    public function storeDepartment()
    {
        $company = Company::findOrFail($this->company);

        $department = $company->departments()->create([
            'name' => $this->name,
            'slug' => str_slug($this->name),
        ]);
    }
}
