<?php

namespace App\Http\Requests\Employee;

use App\Models\Company;
use Illuminate\Http\File;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Http\FormRequest;

class CreateEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company'    => 'required|integer|exists:companies,id',
            'department' => 'required|integer|exists:departments,id',
            'first_name' => 'required|string',
            'last_name'  => 'required|string',
            'email'      => 'required|string|email|max:255|unique:employees,email',
            'phone'      => 'required|phone:MM',
        ];
    }

    public function storeEmployee()
    {
        $company = Company::findOrFail($this->company);

        $employee = $company->employees()->create([
            'first_name'    => $this->first_name,
            'last_name'     => $this->last_name,
            'uuid'          => Str::orderedUuid(),
            'email'         => $this->email,
            'phone'         => $this->phone,
            'department_id' => $this->department
        ]);
    }
}
