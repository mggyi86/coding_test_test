<?php

namespace App\Http\Requests\Company;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'    => 'required|string|unique:companies,name,' . $this->route('company')->id,
            'email'   => 'required|string|email|max:255|unique:companies,email,' . $this->route('company')->id,
            'website' => 'nullable|url',
            'logo'    => 'image|dimensions:min_width=100,min_height=100'
        ];
    }

    public function updateCompany($company)
    {
        $this->uploadCompanyImage($company);

        $company->name    = $this->name;
        $company->email   = $this->email;
        $company->website = $this->website;
        $company->logo    = $this->fileName;

        if($company->isDirty()) {
            $company->slug = str_slug($this->name);
            $company->save();
        }
    }

    public function uploadCompanyImage($company)
    {
        $uploadedImage = $this->logo;
        $this->fileName = $company->logo;

        if($uploadedImage) {
            Storage::delete('public/companies/' . $company->logo);
            $this->fileName = basename(Storage::putFile('public/companies', new File($uploadedImage)));
        }

        return $this;
    }
}
