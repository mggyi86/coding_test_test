<?php

namespace App\Http\Responses;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Contracts\Support\Responsable;

class UserIndexResponse implements Responsable
{
    protected $users;

    public function __construct(Collection $users)
    {
        $this->users = $users;
    }

    public function toResponse($request)
    {
        if (request()->ajax()) {

            return DataTables::of($this->users)->addIndexColumn()
                    ->addColumn('action', function ($user) {
                        return '<a href="/backend/users/'. $user->uuid.'"
                                class="btn btn-sm btn-success"><i class="glyphicon glyphicon-eye-open"></i> </a>

                                <a href="/backend/users/'.$user->uuid.'/edit"
                                class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-edit"></i> </a>

                                <button data-remote="/backend/users/'.$user->uuid.'"
                                class="btn btn-sm btn-danger btn-delete"><i class="glyphicon glyphicon-trash"></i></button>';
                    })
                    ->make(true);
        }

        return view('backend.users.index');
    }
}
