<?php

use App\Models\Company;
use App\Models\Employee;
use App\Models\Department;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Employee::class, function (Faker $faker) {
    $department = Department::all()->random();
    return [
        'first_name'    => $faker->name,
        'last_name'     => $faker->name,
        'uuid'          => Str::orderedUuid(),
        'email'         => $faker->unique()->safeEmail,
        'phone'         => $faker->e164PhoneNumber,
        'company_id'    => $department['company_id'],
        'department_id' => $department['id']
    ];
});
