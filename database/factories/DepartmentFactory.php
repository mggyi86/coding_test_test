<?php

use App\Models\Company;
use App\Models\Department;
use Faker\Generator as Faker;

$factory->define(Department::class, function (Faker $faker) {
    $name = $faker->unique()->name;
    return [
        'name'    => $name,
        'slug'    => str_slug($name),
        'company_id'    => function() {
            return Company::all()->random();
        }
    ];
});
