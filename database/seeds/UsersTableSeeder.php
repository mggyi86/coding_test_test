<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use App\Traits\TruncateTableSeeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    use TruncateTableSeeder;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncate('users');
        $this->truncate('roles');
        $this->truncate('model_has_roles');

        $admin_role = Role::create(['name' => 'admin']);
        $user_role = Role::create(['name' => 'user']);

        //for admin
        $admin = factory(User::class)->create([
                    'name' => 'Admin',
                    'email' => 'admin@admin.com',
                    'password' => Hash::make('password'),
                    'group' => 'admin'
                 ]);
        $admin->assignRole($admin_role );

        //for super admin
        $superadmin = factory(User::class)->create([
                    'name' => 'Super Admin',
                    'email' => 'superadmin@admin.com',
                    'password' => Hash::make('password'),
                    'group' => 'superadmin'
                 ]);
        $superadmin->assignRole($admin_role );

        //for user
        $user = factory(User::class)->create([
                        'name' => 'User',
                        'email' => 'user@user.com',
                        'password' => Hash::make('useruser')
                    ]);
        $user->assignRole($user_role );

        for($i=0; $i<30; $i++) {
            $user = factory(User::class)->create();
            $user->assignRole($user_role);
        }


        //for user
        factory(User::class, 60)->create();
    }
}
