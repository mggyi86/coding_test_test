<?php

use App\Models\Company;
use Illuminate\Database\Seeder;
use App\Traits\TruncateTableSeeder;

class CompaniesTableSeeder extends Seeder
{
    use TruncateTableSeeder;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncate('companies');

        factory(Company::class, 10)->create();
    }
}
