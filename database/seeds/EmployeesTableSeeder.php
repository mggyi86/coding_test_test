<?php

use App\Models\Employee;
use App\Models\Department;
use Illuminate\Database\Seeder;
use App\Traits\TruncateTableSeeder;

class EmployeesTableSeeder extends Seeder
{
    use TruncateTableSeeder;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncate('employees');

        $departments = Department::all();

        factory(Employee::class, 50)->create();

        foreach($departments as $department) {
            factory(Employee::class, 10)->create([
                'company_id'    => $department->company_id,
                'department_id' => $department->id
            ]);
        }
    }
}
